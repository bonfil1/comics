/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function(){
    
    var db = localStorage;
    var users = JSON.parse(db.getItem("users"));   
    var template = ""; 
    var table = document.getElementById("usersBody");
    console.log(users);
    users.forEach(function(user) {
        user = JSON.parse(user);
        template += "<tr><th scope='row'>"+ user.id +"</th><td>"+user.name+"</td><td>"+user.lastname+"</td><td>"+user.role+"</td><td><button class='btn btn-danger delete-user' data-userid='"+user.id+"'>Delete</button></td></tr>";
    });
    
    table.innerHTML = template;
    
    // Delete
    // Adding live element even binding
    document.addEventListener('click', function (e) {
        if (hasClass(e.target, 'delete-user')) {
            var userId = e.target.dataset.userid;
            if( userId )
            {
                console.log(users[userId - 1]);
                delete users[userId - 1];
                console.log(users);
                db.setItem("users", JSON.stringify(users));                
            }            
        }
    }, false);
    // Get the class for the live event
    function hasClass(elem, className) {
        return elem.className.split(' ').indexOf(className) > -1;
    }
    
})();