/* 
 * ComicStore Application main JS
 */
(function () {

    // Dealing with the User Obj
    var register = document.getElementById("register");
    var login = document.getElementById("login");
    var logout = document.getElementById("logout");
    var navBar = document.getElementById("nav-user");
    var userNameInBar = document.getElementById("userNameBar");
    var adminOptions = document.querySelectorAll(".admin");
    var detailComicView = document.querySelectorAll(".detail");
    var search = document.getElementById("search");
    var topComics = "";
    var id = 0;
    var name = "";
    var lastName = "";
    var pass = "";
    var role = "";
    var users = [];
    var usersCount;
    var lastObject;

    // Checking for localStorage informaction
    db = localStorage;

    // Comic items paint on the front page -- Start
    function initCommics(jsonSource, tabLayout, layoutType) {
        
        // Add the filter list
        var list = "<div class='row'>";
        list += "<ul class='list-group'>";
        var genres = jsonSource.map(function(value, index) {return value['genre']});
        var editions = jsonSource.map(function(value, index) {return value['year']});
        var genreCount = {};
        var editionCount = {};
        
        // Remove duplicated items
        if (layoutType == "genre") 
        {
            genres.forEach(function(value, index) {
                if (value in genreCount) {
                    genreCount[value] += 1;
                } else {
                    genreCount[value] = 1;
                }
            });
            
            for( var genre in genreCount ){
                if (genreCount.hasOwnProperty(genre)) {
                    console.log(genre + " -> " + genreCount[genre]);
                    list += "<li class='list-group-item'>";
                    list +=     "<span class='badge'>"+ genreCount[genre] +"</span>";
                    list +=     genre;
                    list += "</li>";
                }
            }          
        }
        
        if (layoutType == "editions") 
        {
            editions.forEach(function(value, index) {
                if (value in editionCount) {
                    editionCount[value] += 1;
                } else {
                    editionCount[value] = 1;
                }
            });
            
            for( var edition in editionCount ){
                if (editionCount.hasOwnProperty(edition)) {
                    console.log(edition + " -> " + editionCount[edition]);
                    list += "<li class='list-group-item'>";
                    list +=     "<span class='badge'>"+ editionCount[edition] +"</span>";
                    list +=     edition;
                    list += "</li>";
                }
            }          
        }
        
        // Loop the Changes
        var template = "<div class='row'>";
        jsonSource.forEach(function (comic) {
            // Comics list
            template += "<div class='col-sm-4 col-md-4'><div class='item'>";
            template += "<div class='thumbnail'>" +
                    "<img src=" + comic.img + " alt=" + comic.name + " class='img-responsive'>" +
                    "<div class='caption'>" +
                    "<h3>" + comic.name + "</h3>";

            if (layoutType == "all")
            {
                template += "<p>" + comic.desc + "</p>";
            } else if (layoutType == "genre")
            {
                template += "<p>" + comic.genre + "</p>";
            } else if (layoutType == "editions")
            {
                template += "<p>" + comic.trademark + "</p>";
                template += "<p>" + comic.year + "</p>";
            }
            template += "<p>" +
                    "<a href='#' class='btn btn-primary' role='button'>Buy</a>" +
                    "<a href='pages/detail.html' class='btn btn-info detail' data-view='" + comic.id + "' role='button'>View</a>" +
//                                "<a href='#' class='btn btn-info detail' data-view='" + comic.id + "' role='button'>View</a>"+
                    "<a href='#' class='btn btn-success' role='button'>Rent</a>" +
                    "</p>" +
                    "</div>" +
                    "</div>";
            template += "</div></div>";
        });
        template += "</div>";   // Closing comics layout
        list += "</ul></div>";   // Closing list
        if (tabLayout) {
            tabLayout.innerHTML = list + template;
        }

    }
    // Comic items paint on the front page -- End

    // On Register Logic
    if (register)
    {
        register.onsubmit = function () {
            document.getElementById("message").classList.add("hide");
            document.getElementById("message-ok").classList.add("hide");

            name = document.getElementById("name").value;
            lastName = document.getElementById("lastname").value;
            pass = document.getElementById("password").value;
            role = "user";

            // Creating User obj
            var user = {};
            users = JSON.parse(db.getItem("users")); // to assign the ID of the user we make a count

            if (users)
            {
                usersCount = Object.keys(users).length;
                id = parseInt(usersCount) + 1;
                console.log("ID: " + id);
            } else
            {
                users = [];
                id = 1;
            }
            // Validating before persisting
            if (name !== null)
            {
                var validName = false;
                users.forEach(function (user) {
                    var u = JSON.parse(user);
                    console.log(u.name);
                    if (u.name == name)
                    {
                        console.log("This user aleady exist, please use another!");
                        document.getElementById("message").classList.remove("hide");
                    } else
                    {
                        validName = true;
                    }
                });

                if (validName) {
                    user = new User(id, name, lastName, pass, role);
                    console.log(user);
                    users.push(JSON.stringify(user));
                    db.setItem("users", JSON.stringify(users));
                    document.getElementById("message-ok").classList.remove("hide");
                }
            }

            console.log(JSON.parse(db.getItem("users")));

            return false;
        }
    }
    // On Register Logic

    // On Login Logic
    if (login)
    {
        login.onsubmit = function () {
            name = document.getElementById("username").value;
            pass = document.getElementById("userpassword").value;
            var success = false;
            // Creating User obj
            users = JSON.parse(db.getItem("users")); // to assign the ID of the user we make a count

            // Validating before persisting            
            for (var i = 0; i < users.length; i++) {
                var obj = JSON.parse(users[i]);

                if (obj.name == name && obj.password == pass)
                {
                    //alert("Success!!");
                    console.log("Success!!");   // Urraah!
                    db.setItem("active", name); // Setting localStorage flag with user name
                    db.setItem("active_user_role", obj.role);
                    db.setItem("active_user", JSON.stringify(obj));

                    jQuery("#loginModal").modal('toggle');  // Hide modal on success
                    identifyUser(); // Show admin bar

                    break;
                } else
                {
                    //alert("Error!!");
                    db.removeItem("active");
                    db.removeItem("active_user_role");
                    console.log("Error!!");
                }
            }

            return false;
        }
    }
    // On Login Logic

    // Step 3 - Show username on navbar
    function identifyUser()
    {
        if (db.getItem("active") != null)
        {
            if (navBar && userNameInBar)
            {
                userNameInBar.innerHTML = db.getItem("active");
                navBar.style.display = 'block';
                if (db.getItem("active_user_role") == "admin")
                {
                    adminOptions.forEach(function (el) {
                        el.classList.remove("hide");
                    });
                } else {
                    document.getElementById("userprofile").classList.remove("hide");
                }
            }
        }
    }


    if (logout)
    {
        logout.onclick = function () {
            db.removeItem("active");
            db.removeItem("active_user_role");
            db.removeItem("active_user");

            if (navBar && userNameInBar)
            {
                userNameInBar.innerHTML = "";
                navBar.style.display = 'none';
                adminOptions.forEach(function (el) {
                    el.classList.add("hide");
                });

                document.getElementById("userprofile").classList.add("hide");
            }
        };
    }

    if (detailComicView)
    {
        detailComicView.forEach(function (el) {
            el.onclick = function () {
                console.log("click on this item: " + el.dataset.view);
                db.setItem("comic_view", el.dataset.view);
            };
        });
    }

    if (search)
    {
        search.onsubmit = function () {
            var q = document.getElementById("query").value;
            var results;
            var o = JSON.parse(db.getItem("comicsList"));

            results = searchByName(q, o);

            if (!jQuery.isEmptyObject(results))
            {
                console.log("Search by name");
//                console.log(results);
            } else if (!jQuery.isEmptyObject(searchByGenre(q, o)))
            {
                console.log("Searching by genre");
                results = searchByGenre(q, o);
//                console.log(results);
            } else if (!jQuery.isEmptyObject(searchByTrademark(q, o)))
            {
                console.log("Searching by trademark");
                results = searchByTrademark(q, o);
//                console.log(results);
            }

            if (results)
            {
                var body = document.querySelector(".body-comics");
                body.innerHTML = "";
                var template = "<div class='row'><div class='col-md-10'>";
                results.forEach(function (r) {
                    template += "<div class='col-sm-4 col-md-4'><div class='item'>";
                    template += "<div class='thumbnail'>" +
                            "<img src=" + r.img + " alt=" + r.name + " class='img-responsive'>" +
                            "<div class='caption'>" +
                            "<h3>" + r.name + "</h3>" +
                            "<p>" + r.desc + "</p>" +
                            "<p>" +
                            "<a href='#' class='btn btn-primary' role='button'>Buy</a>" +
                            "<a href='pages/detail.html' class='btn btn-info detail' data-view='" + 0 + "' role='button'>View</a>" +
                            "<a href='#' class='btn btn-success' role='button'>Rent</a>" +
                            "</p>" +
                            "</div>" +
                            "</div>";
                    template += "</div></div>";
                });
                template += "</div></div>";
                body.innerHTML = template;
            }
            return false;
        }
    }

    // Load .json file
    function init() {
        loadJSON(function (response) {
            // Parse JSON string into object
            topComics = JSON.parse(response);
            var ul = document.getElementById("topComicsList");

            // Printing top comics on the front
            for (var i = 0; i < topComics.length; i++) {
                var comic = topComics[i];
                var li = document.createElement("li");
                var children = ul.children.length + 1
                li.setAttribute("id", "element" + children)
                li.appendChild(document.createTextNode(comic.name + " - " + comic.trademark)); // Name of the comic
                ul.appendChild(li)
            }
        }, "resources/topcomics.json");
    }

    function loadComicsList() {
        loadJSON(function (response) {
            topComics = JSON.parse(response);
            db.setItem("comicsList", JSON.stringify(topComics));
        }, "resources/comics.json");
    }

    // Adding live element even binding
    document.addEventListener('click', function (e) {
        if (hasClass(e.target, 'detail')) {
            db.setItem("comic_view", e.target.dataset.view);
            console.log("click on this item: " + e.target.dataset.view);
        }
    }, false);
    // Get the class for the live event
    function hasClass(elem, className) {
        return elem.className.split(' ').indexOf(className) > -1;
    }

    // Functions on begining
    init();
    loadComicsList();
    identifyUser();


    var comicList = JSON.parse(db.getItem("comicsList"));
    var tabContent = {};
    var tabAllComics = document.getElementById("all");
    var tabGenresComics = document.getElementById("genres");
    var tabEditionsComics = document.getElementById("editions");
    var tabNewsComics = document.getElementById("news");
    var tabCharactersComics = document.getElementById("characters");

    // We use the same function to paint the comic list regarding the tab
    // I'm using a filter function to pass the json list
    initCommics(comicList, tabAllComics, "all");  // All

    //tabContent = searchByGenre("all", comicList);           // Genre filter
    initCommics(comicList, tabGenresComics, "genre");       // Genre

    initCommics(comicList, tabEditionsComics, "editions");  // Editions
})();

