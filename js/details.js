/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function(){
    
    var db = localStorage;
    var viewID = db.getItem("comic_view");
    
    if( viewID )
    {
        console.log( "You are watching the details of " + viewID );
        // Functions on begining
        init();        
    }
    
    
    
    // Load .json file
    function init() {
        loadJSON(function (response) {
            // Parse JSON string into object
            var topComics = JSON.parse(response);
            var comicImg = document.getElementById("comic-img");
            var comicTitle = document.getElementById("comicTitle");
            var comicDesc = document.getElementById("comicDesc");
            
            // Printing top comics on the front
            var comic = topComics[ db.getItem("comic_view") ];
            comicImg.setAttribute("src", comic.img);
            comicImg.setAttribute("alt", comic.name);
            comicTitle.innerHTML = comic.name;
            comicDesc.innerHTML = comic.desc;
            
            db.setItem( "view", JSON.stringify(comic) );            
            console.log( db.getItem("view") );
        }, "/comics/resources/comics.json");
    }        
})();

