/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function(){
    
    var db = localStorage;
    var user = JSON.parse(db.getItem("active_user"));
    var userForm;
    
    if (user)
    {
        userForm = document.getElementById("user-update");
        var name = document.getElementById("username");
        var lastName = document.getElementById("lastName");
        var pass = document.getElementById("password");
        
        name.value = user.name;
        lastName.value = user.lastname;
        pass.value = user.password;
        
        userForm.onsubmit = function(){            
            var users = JSON.parse(db.getItem("users"));
            
            var updateUser = JSON.parse(users[user.id - 1]);
            updateUser.name = name.value;
            updateUser.lastname = lastName.value;
            updateUser.password = pass.value;
            
            users[user.id -1] = JSON.stringify(updateUser);
            console.log(users);
            db.setItem( "users", JSON.stringify(users) );
            db.setItem("active", updateUser.name);
            db.setItem("active_user", JSON.stringify(updateUser));
            return false;
        };
        
    }
})();

