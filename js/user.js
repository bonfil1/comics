/* 
 * User Object
 */
function User(id, name, lastname, password, role)
{
    this.id = id;
    this.name = name;
    this.lastname = lastname;
    this.password = password;
    this.role = role;
    
    this.getId = function()
    {
        return this.id;
    }
    this.getName = function()
    {
        return this.name;
    }
    this.getLastName = function()
    {
        return this.lastname;
    }
    this.getPassword = function()
    {
        return this.password;
    }
    this.getRole = function()
    {
        return this.role;
    }
}


