/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function loadJSON(callback, filename) {

    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', filename, true); // Replace 'my_data' with the path to your file
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
            // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
            callback(xobj.responseText);
        }
    };
    xobj.send(null);
}

function searchByName( query, obj ) {
    
    if ( obj )
    {
        return obj.filter(function(o) {
            return o.name.includes(query);           
        });
    }
    else
    {
        return null;
    }
}

function searchByGenre( query, obj ) {
    
    var results;
    if ( obj )
    {
        results = obj.filter(function(o) {
            return o.genre.includes(query);           
        });
        return results;
    }
    else
    {
        return null;
    }
}

function searchByTrademark( query, obj ) {
    
    var results;
    if ( obj )
    {
        results = obj.filter(function(o) {
            return o.trademark.includes(query);           
        });
        return results;
    }
    else
    {
        return null;
    }
}
